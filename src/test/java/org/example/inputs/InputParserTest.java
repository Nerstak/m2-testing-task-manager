package org.example.inputs;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class InputParserTest {
    @Test
    void testAddTask() {
        Action res = InputParser.evaluateAction("+ Learn Python");

        assertEquals(Action.ADD, res, "Add task is parsed");
    }

    @Test
    void testRemoveTask() {
        Action res = InputParser.evaluateAction("- 1");

        assertEquals(Action.REMOVE, res, "Remove task is parsed");
    }

    @Test
    void testDoneTask() {
        Action res = InputParser.evaluateAction("x 1");

        assertEquals(Action.TO_DONE, res, "Done task is parsed");
    }

    @Test
    void testToDoTask() {
        Action res = InputParser.evaluateAction("o 1");

        assertEquals(Action.TO_TODO, res, "ToDo task is parsed");
    }

    @Test
    void testQuit() {
        Action res = InputParser.evaluateAction("q");

        assertEquals(Action.QUIT, res, "Quit is parsed");
    }

    @Test
    void testGetDescriptionTask() {
        String res = InputParser.evaluateDescription("+ Learn Python");

        assertEquals("Learn Python", res, "org.example.tasks.Task description is parsed");
    }

    @Test
    void testGetIdTask() {
        int res = InputParser.evaluateId("x 1");

        assertEquals(1, res, "ID's task is parsed");
    }
}