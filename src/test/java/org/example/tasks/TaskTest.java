package org.example.tasks;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TaskTest {
    @AfterEach
    void clean() {
        Task.getList().clear();
    }

    @Test
    void testNewTask() {
        new Task("Learn Python");

        Task recentTask = Task.getList().get(Task.getList().size() - 1);
        Assertions.assertEquals("Learn Python", recentTask.getDescription(), "Description is correctly identified");
    }

    @Test
    void testUpdateStatus() {
        new Task("Learn Python");

        Task.updateStatusFromList(0, TaskStatus.DONE);

        Task recentTask = Task.getList().get(Task.getList().size() - 1);
        Assertions.assertEquals(TaskStatus.DONE, recentTask.getStatus(), "Status was updated in list");
    }

    @Test
    void testRemoveTask() {
        new Task("Learn Python");

        Task.removeFromList(0);

        Assertions.assertEquals(0, Task.getList().size(), "org.example.tasks.Task is removable from list");
    }

    @Test
    void testPrettyToStringToDo() {
        Task t = new Task("Learn Python");

        String s = t.toStringPretty();

        assertEquals("[ ] Learn Python", s, "Pretty toString for single ToDo element has correct formatting");
    }

    @Test
    void testPrettyToStringDone() {
        Task t = new Task("Learn Python");
        t.setStatus(TaskStatus.DONE);

        String s = t.toStringPretty();

        assertEquals("[x] Learn Python", s, "Pretty toString for single Done element has correct formatting");
    }

    @Test
    void testPrettyToStringList() {
        Task t = new Task("Learn Python");
        new Task("Learn TDD");
        t.setStatus(TaskStatus.DONE);

        String s = Task.getListToString();

        assertEquals("0 [x] Learn Python\n" +
                "1 [ ] Learn TDD",
                s,
                "Display of list of task has correct formatting");
    }
}