package org.example.tasks;

import org.example.Program;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TaskManagerHandlingTest {
    @AfterEach
    void clean() {
        Task.getList().clear();
    }

    @Test
    void testCreateTask() {
        String input = "+ Learn Python";

        TaskManagerHandling.evaluate(input);

        assertTrue(Task.getList().size() == 1 && Task.getList().get(0).getDescription().equals("Learn Python"), "org.example.tasks.Task was created");
    }

    @Test
    void testRemoveTask() {
        new Task("Learn Python");
        String input = "- 0";

        TaskManagerHandling.evaluate(input);

        Assertions.assertEquals(0, Task.getList().size(), "org.example.tasks.Task was removed");
    }

    @Test
    void testMarkDoneTask() {
        new Task("Learn Python");
        String input = "x 0";

        TaskManagerHandling.evaluate(input);

        assertTrue(Task.getList().size() == 1 && Task.getList().get(0).getStatus() == TaskStatus.DONE, "org.example.tasks.Task was marked as done");
    }

    @Test
    void testMarkToDoTask() {
        Task t = new Task("Learn Python");
        t.setStatus(TaskStatus.DONE);
        String input = "o 0";

        TaskManagerHandling.evaluate(input);

        assertTrue(Task.getList().size() == 1 && Task.getList().get(0).getStatus() == TaskStatus.TODO, "org.example.tasks.Task was marked as todo");
    }
}