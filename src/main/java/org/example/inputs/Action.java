package org.example.inputs;

public enum Action {
    ADD,
    REMOVE,
    TO_DONE,
    TO_TODO,
    QUIT
}
