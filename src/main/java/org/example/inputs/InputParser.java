package org.example.inputs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Parse input
 */
public final class InputParser {
    /**
     * Evaluate a string and give correct action
     * @param s Input to parse
     * @return org.example.inputs.Action identified
     */
    public static Action evaluateAction(String s) {
        List<String> strings = toStringList(s);
        String action = strings.get(0);
        return switch (action) {
            case "+" -> Action.ADD;
            case "-" -> Action.REMOVE;
            case "x" -> Action.TO_DONE;
            case "o" -> Action.TO_TODO;
            case "q" -> Action.QUIT;
            default -> throw new IllegalStateException("Unexpected value: " + action);
        };
    }

    /**
     * Evaluate task description of input
     * @param s input
     * @return Description
     */
    public static String evaluateDescription(String s) {
        List<String> descArray = toStringList(s);
        descArray.remove(0);
        return String.join(" ", descArray);
    }

    /**
     * Convert input to List of string (separation by space)
     * @param s Input
     * @return List of string
     */
    private static List<String> toStringList(String s) {
        return new ArrayList<>(Arrays.stream(s.split(" ")).toList());
    }

    /**
     * Evaluate task id of an input
     * @param s Input
     * @return ID
     */
    public static int evaluateId(String s) throws NumberFormatException {
        List<String> strings = toStringList(s);
        String id = strings.get(1);

        return Integer.parseInt(id);
    }
}
