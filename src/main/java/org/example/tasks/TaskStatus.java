package org.example.tasks;

/**
 * Status of a task
 */
public enum TaskStatus {
    DONE,
    TODO
}
