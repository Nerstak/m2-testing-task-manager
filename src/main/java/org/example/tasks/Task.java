package org.example.tasks;

import java.util.LinkedList;
import java.util.List;

/**
 * org.example.tasks.Task, including a shared list of task
 */
public class Task {
    /**
     * Global list of tasks
     */
    private static final List<Task> list = new LinkedList<>();

    /**
     * Description of task
     */
    private final String description;

    /**
     * Status of the task
     */
    private TaskStatus status = TaskStatus.TODO;

    public Task(String description) {
        this.description = description;
        list.add(this);
    }

    /**
     * Safe method to remove an element from list
     * @param i Index of element to remove
     */
    public static void removeFromList(int i) {
        if(indexWithinBoundaries(i)) {
            list.remove(i);
        }
    }

    /**
     * Safe method to update status of an element from list
     * @param i Index of element
     * @param status New status
     */
    public static void updateStatusFromList(int i, TaskStatus status) {
        if(indexWithinBoundaries(i)) {
            list.get(i).status = status;
        }
    }

    /**
     * Check if index is within boundaries
     * @param index Index to check
     * @return Verification status
     */
    private static boolean indexWithinBoundaries(int index) {
        return index >= 0 && index < list.size();
    }

    /**
     * Get pretty string of list of task
     * @return String
     */
    public static String getListToString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append(i).append(" ").append(list.get(i).toStringPretty());
            if(i != list.size() - 1) sb.append("\n");
        }

        return sb.toString();
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public static List<Task> getList() {
        return list;
    }

    public String getDescription() {
        return description;
    }

    /**
     * org.example.tasks.Task to String, but with prettier formatting
     * @return String
     */
    public String toStringPretty() {
        StringBuilder sb = new StringBuilder("[");
        sb.append(status == TaskStatus.DONE ? "x" : " ");
        sb.append("] ");
        sb.append(description);

        return sb.toString();
    }
}
