package org.example.tasks;

import org.example.inputs.Action;
import org.example.inputs.InputParser;

public final class TaskManagerHandling {
    /**
     * Handling based on input
     * @param input String
     */
    public static void evaluate(String input) {
        Action action = InputParser.evaluateAction(input);
        switch (action) {
            case ADD -> new Task(InputParser.evaluateDescription(input));
            case REMOVE -> Task.removeFromList(InputParser.evaluateId(input));
            case TO_DONE -> Task.updateStatusFromList(InputParser.evaluateId(input), TaskStatus.DONE);
            case TO_TODO -> Task.updateStatusFromList(InputParser.evaluateId(input), TaskStatus.TODO);
            default -> throw new IllegalStateException("Unexpected value: " + action);
        }
    }
}
