package org.example;

import org.example.inputs.Action;
import org.example.inputs.InputParser;
import org.example.tasks.Task;

import java.util.Scanner;

import static org.example.tasks.TaskManagerHandling.evaluate;

public class Program {
    public static void main (String[] args) {
        boolean run = true;
        Scanner scanner = new Scanner(System.in);
        do {
            String input = scanner.nextLine();
            if(InputParser.evaluateAction(input) == Action.QUIT) {
                run = false;
                System.out.println("Bye!");
            } else {
                evaluate(input);
                System.out.println(Task.getListToString());
            }
        } while(run);
    }


}
